from email.mime.text import MIMEText

def format_reminder_email(quest_id: str,
                          days_signed_out: int,
                          librarian_name: str,
                          book_name) -> str:
    """
    Formats an email as a plain string for sending out email reminders
    for signed out books.

    Example: format_reminder_email("s455wang", 30, "Connor Murphy", "How to Design Programs")
    """
    assert len(quest_id) <= 8
    assert quest_id.isalnum()
    assert days_signed_out > 0
    assert librarian_name != ""
    assert book_name != ""

    email_message = MIMEText(
"""Hi {},

Our records indicate that you have had the book {} signed out for {} days.

If you would like to keep this book checked out, tell us when in the next month you will return this book.

If you think you have received this message in error, reply back to this email please!

Otherwise, please return the book to the CS Club office (MC 3036) at your earliest convenience.

Thank you for using the CS Club library!

{} | Librarian
Computer Science Club | University of Waterloo
librarian@csclub.uwaterloo.ca""".format(
        quest_id,
        book_name,
        days_signed_out,
        librarian_name
    ))

    email_message["Subject"] = "Overdue book: {}".format(book_name)
    email_message["To"] = "\"{0}@csclub.uwaterloo.ca\" <{0}@csclub.uwaterloo.ca>\n".format(quest_id)
    assert email_message.as_string().replace("\n", "").isprintable(), \
        "Our email should not have characters apart from normal characters and newline"
    return email_message.as_string()

